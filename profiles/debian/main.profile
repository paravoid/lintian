# This profile is auto-generated
Profile: debian/main
Extends: debian/ftp-master-auto-reject
Enable-Tags-From-Check: apache2, application-not-library, appstream-metadata, automake,
 binaries, changelog, changes-file, conffiles, control-file, control-files,
 copyright, cruft, dbus, deb-format, debconf, debhelper, debian-readme,
 debian-source-dir, description, duplicate-files, elpa, fields, filename-length,
 files, gir, group-checks, huge-usr-share, infofiles, init.d, java, lintian,
 manpages, md5sums, menu-format, menus, nmu, nodejs, obsolete-sites, ocaml,
 patch-systems, pe, phppear, po-debconf, python, rules, scripts, shared-libs,
 standards-version, symlinks, systemd,
 testsuite, triggers, udev, upstream-metadata, upstream-signing-key, usrmerge,
 version-substvars, watch-file

